// Copyright 2022 Ledger A LLC, All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package hdbscan

import (
	"sort"
	"testing"
)

var (
	data = [][]float64{
		// cluster-1 (0-7)
		[]float64{1, 2, 3},
		[]float64{1, 2, 4},
		[]float64{1, 2, 5},
		[]float64{1, 3, 4},
		[]float64{2, 3, 3},
		[]float64{2, 2, 4},
		[]float64{2, 2, 5},
		[]float64{2, 3, 4},
		// cluster-2 (8-15)
		[]float64{21, 15, 6},
		[]float64{22, 15, 5},
		[]float64{23, 15, 7},
		[]float64{24, 15, 8},
		[]float64{21, 15, 6},
		[]float64{22, 16, 5},
		[]float64{23, 17, 7},
		[]float64{24, 18, 8},
		// cluster-3 (16-23)
		[]float64{80, 85, 90},
		[]float64{89, 90, 91},
		[]float64{100, 100, 100}, // possible outlier
		[]float64{90, 90, 90},
		[]float64{81, 85, 90},
		[]float64{89, 91, 91},
		[]float64{100, 101, 100}, // possible outlier
		[]float64{90, 91, 90},
		// outlier
		[]float64{-2400, 2000, -30},
	}
	minimumClusterSize = 3
)

func TestBuildClusters(t *testing.T) {
	clustering, err := NewClustering(data, minimumClusterSize)
	if err != nil {
		t.Errorf("clustering creation error: %+v", err)
	}
	clustering.distanceFunc = EuclideanDistance
	// clustering.minTree = true

	// cluster-hierarchy
	dendrogram := clustering.buildDendrogram(clustering.mutualReachabilityGraph())
	clustering.buildClusters(dendrogram)

	for _, cluster := range clustering.Clusters {
		sort.Ints(cluster.Points)
		t.Logf("Cluster %+v with points: %+v", cluster.id, cluster.Points)
	}
}

func TestVarianceClustering(t *testing.T) {
	clustering, err := NewClustering(data, minimumClusterSize)
	if err != nil {
		t.Errorf("clustering creation error: %+v", err)
	}

	err = clustering.Run(EuclideanDistance, VarianceScore, true)
	if err != nil {
		t.Errorf("clustering run error: %+v", err)
	}

	for _, cluster := range clustering.Clusters {
		sort.Ints(cluster.Points)
		t.Logf("Cluster %+v with points: %+v", cluster.id, cluster.Points)
	}
}

func TestStabilityClustering(t *testing.T) {
	clustering, err := NewClustering(data, minimumClusterSize)
	if err != nil {
		t.Errorf("clustering creation error: %+v", err)
	}

	err = clustering.Run(EuclideanDistance, StabilityScore, true)
	if err != nil {
		t.Errorf("clustering run error: %+v", err)
	}

	for _, cluster := range clustering.Clusters {
		sort.Ints(cluster.Points)
		t.Logf("Cluster %+v with points: %+v", cluster.id, cluster.Points)
	}
}

func TestClusteringNoTree(t *testing.T) {
	clustering, err := NewClustering(data, minimumClusterSize)
	if err != nil {
		t.Errorf("clustering creation error: %+v", err)
	}

	err = clustering.Run(EuclideanDistance, VarianceScore, false)
	if err != nil {
		t.Errorf("clustering run error: %+v", err)
	}

	for _, cluster := range clustering.Clusters {
		sort.Ints(cluster.Points)
		t.Logf("Cluster %+v with points: %+v", cluster.id, cluster.Points)
	}
}

func TestClusteringVerbose(t *testing.T) {
	c, err := NewClustering(data, minimumClusterSize)
	if err != nil {
		t.Errorf("clustering creation error: %+v", err)
	}
	c = c.Verbose()

	err = c.Run(EuclideanDistance, VarianceScore, false)
	if err != nil {
		t.Errorf("clustering run error: %+v", err)
	}

	for _, cluster := range c.Clusters {
		sort.Ints(cluster.Points)
		t.Logf("Cluster %+v with points: %+v", cluster.id, cluster.Points)
	}
}
