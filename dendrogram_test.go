// Copyright 2022 Ledger A LLC, All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package hdbscan

import "testing"

func TestBuildDendrogram(t *testing.T) {
	clustering, err := NewClustering(data, minimumClusterSize)
	if err != nil {
		t.Errorf("clustering creation error: %+v", err)
	}
	clustering.distanceFunc = EuclideanDistance
	clustering.minTree = true

	// cluster-hierarchy
	dendrogram := clustering.buildDendrogram(clustering.mutualReachabilityGraph())

	for _, link := range dendrogram {
		t.Logf("Link %+v with points: %+v", link.id, link.points)
	}
}
