module gitlab.com/ledgera/hdbscan

go 1.16

require (
	gitlab.com/ledgera/sam v0.0.0-20220403015307-3970e848870c
	gonum.org/v1/gonum v0.11.0
)
