// Copyright 2022 Ledger A LLC, All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package hdbscan

import (
	"sort"
	"testing"
)

func TestClusteringSamplingAndAssignAndOutlierClustering(t *testing.T) {
	c, err := NewClustering(data, minimumClusterSize)
	if err != nil {
		t.Errorf("clustering creation error: %+v", err)
	}
	c = c.Subsample(16).NearestNeighbor().OutlierClustering()

	err = c.Run(EuclideanDistance, VarianceScore, true)
	if err != nil {
		t.Errorf("clustering run error: %+v", err)
	}

	newClustering, err := c.Assign(data)
	if err != nil {
		t.Errorf("clustering run error: %+v", err)
	}

	for _, cluster := range newClustering.Clusters {
		sort.Ints(cluster.Points)
		t.Logf("Cluster %+v with points: %+v", cluster.id, cluster.Points)
	}
}

func TestClusteringOutliers(t *testing.T) {
	c, err := NewClustering(data, minimumClusterSize)
	if err != nil {
		t.Errorf("clustering creation error: %+v", err)
	}

	c = c.OutlierDetection().NearestNeighbor()

	err = c.Run(EuclideanDistance, VarianceScore, true)
	if err != nil {
		t.Errorf("clustering run error: %+v", err)
	}

	for _, cluster := range c.Clusters {
		sort.Ints(cluster.Points)
		t.Logf("Cluster %+v with Points %+v and outliers: %+v", cluster.id, cluster.Points, cluster.Outliers)
	}
}

func TestClusteringVoronoi(t *testing.T) {
	c, err := NewClustering(data, minimumClusterSize)
	if err != nil {
		t.Errorf("clustering creation error: %+v", err)
	}
	c = c.Verbose().Voronoi()

	err = c.Run(EuclideanDistance, VarianceScore, true)
	if err != nil {
		t.Errorf("clustering run error: %+v", err)
	}

	for _, cluster := range c.Clusters {
		sort.Ints(cluster.Points)
		t.Logf("Cluster %+v with points: %+v", cluster.id, cluster.Points)
	}
}

func TestClusteringVoronoiParts(t *testing.T) {
	c, err := NewClustering(data, minimumClusterSize)
	if err != nil {
		t.Errorf("clustering creation error: %+v", err)
	}
	c = c.Verbose().Voronoi()
	c.distanceFunc = EuclideanDistance
	c.minTree = true

	edges := c.mutualReachabilityGraph()
	t.Logf("%+v\n", edges)
	dendrogram := c.buildDendrogram(edges)
	for _, link := range dendrogram {
		t.Logf("Link %+v with points: %+v", link.id, link.points)
	}

	c.buildClusters(dendrogram)
	for _, cluster := range c.Clusters {
		sort.Ints(cluster.Points)
		t.Logf("Cluster %+v with points: %+v", cluster.id, cluster.Points)
	}

	c.scoreClusters(VarianceScore)
	for _, cluster := range c.Clusters {
		sort.Ints(cluster.Points)
		t.Logf("Cluster %+v with variance %+v and score %+v and points: %+v", cluster.id, cluster.variance, cluster.score, cluster.Points)
	}

	c.selectOptimalClustering(VarianceScore)
	for _, cluster := range c.Clusters {
		sort.Ints(cluster.Points)
		t.Logf("Cluster %+v with variance %+v and score %+v and points: %+v", cluster.id, cluster.variance, cluster.score, cluster.Points)
	}

	c.clusterCentroids()
	for _, cluster := range c.Clusters {
		sort.Ints(cluster.Points)
		t.Logf("Cluster %+v with variance %+v and score %+v and points: %+v and Centroid %+v", cluster.id, cluster.variance, cluster.score, cluster.Points, cluster.Centroid)
	}

	c.outliersAndVoronoi()
	for _, cluster := range c.Clusters {
		sort.Ints(cluster.Points)
		t.Logf("Cluster %+v with variance %+v and score %+v and points: %+v and Centroid %+v", cluster.id, cluster.variance, cluster.score, cluster.Points, cluster.Centroid)
	}
}

func TestClusteringVoronoiNoTree(t *testing.T) {
	c, err := NewClustering(data, minimumClusterSize)
	if err != nil {
		t.Errorf("clustering creation error: %+v", err)
	}
	c = c.Verbose().Voronoi()

	err = c.Run(EuclideanDistance, VarianceScore, false)
	if err != nil {
		t.Errorf("clustering run error: %+v", err)
	}

	for _, cluster := range c.Clusters {
		sort.Ints(cluster.Points)
		t.Logf("Cluster %+v with points: %+v", cluster.id, cluster.Points)
	}
}
