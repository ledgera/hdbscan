// Copyright 2022 Ledger A LLC, All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package hdbscan

import (
	"sort"
	"testing"
)

func TestClusteringSampling(t *testing.T) {
	c, err := NewClustering(data, minimumClusterSize)
	if err != nil {
		t.Errorf("clustering creation error: %+v", err)
	}
	c = c.Verbose().Subsample(16)

	err = c.Run(EuclideanDistance, VarianceScore, true)
	if err != nil {
		t.Errorf("clustering run error: %+v", err)
	}

	for _, cluster := range c.Clusters {
		sort.Ints(cluster.Points)
		t.Logf("Cluster %+v with points: %+v", cluster.id, cluster.Points)
	}
}

func TestClusteringSamplingAndAssign(t *testing.T) {
	c, err := NewClustering(data, minimumClusterSize)
	if err != nil {
		t.Errorf("clustering creation error: %+v", err)
	}
	c = c.Subsample(16).OutlierDetection()

	err = c.Run(EuclideanDistance, VarianceScore, true)
	if err != nil {
		t.Errorf("clustering run error: %+v", err)
	}

	newClustering, err := c.Assign(data)
	if err != nil {
		t.Errorf("clustering run error: %+v", err)
	}

	for _, cluster := range newClustering.Clusters {
		sort.Ints(cluster.Points)
		t.Logf("Cluster %+v with points: %+v", cluster.id, cluster.Points)
	}
}
