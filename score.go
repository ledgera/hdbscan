// Copyright 2022 Ledger A LLC, All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package hdbscan

import (
	"gitlab.com/ledgera/sam"
)

func (c *Clustering) scoreClusters(optimization string) {
	switch optimization {
	case VarianceScore:
		c.varianceScores()
	default:
		c.stabilityScores()
	}
}

func (c *Clustering) varianceScores() {
	c.setNormalizedSizes()
	c.setNormalizedVariances()
	c.Clusters.setVarianceScores()
}

func (c clusters) setVarianceScores() {
	for _, cluster := range c {
		cluster.score = cluster.size / cluster.variance
	}
}

func (c *Clustering) setNormalizedSizes() {
	// distro
	var sizes []float64
	for _, cluster := range c.Clusters {
		size := float64(len(cluster.Points))
		sizes = append(sizes, size)
		cluster.size = size
	}
}

func (c *Clustering) setNormalizedVariances() {
	// variances
	var variances []float64
	for _, cluster := range c.Clusters {
		// data
		var clusterData [][]float64
		for _, pointIndex := range cluster.Points {
			clusterData = append(clusterData, c.data[pointIndex])
		}

		variance := GeneralizedVariance(len(cluster.Points), len(clusterData[0]), unfold(clusterData))
		cluster.variance = isNum(variance)
		variances = append(variances, cluster.variance)
	}
}

func (c *Clustering) stabilityScores() {
	for _, cl := range c.Clusters {
		c.semaphore <- true
		c.wg.Add(1)
		go func(cl *cluster) {
			pointDistances := make(sam.MapIntFloat64)
			for _, pIndex := range cl.Points {
				for _, edge := range c.mst.edges {
					if edge.p1 == pIndex {
						pointDistances[pIndex] = edge.dist
					}
				}
			}

			maxDistance := pointDistances.MaxValue()
			birthLambda := 1 / maxDistance

			var stability float64
			for _, distance := range pointDistances {
				stability += (1 / distance) - birthLambda
			}
			cl.score = stability
			<-c.semaphore
			c.wg.Done()
		}(cl)
	}
	c.wg.Wait()
}
